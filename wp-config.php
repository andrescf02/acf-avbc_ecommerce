<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ecommerce');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Zb7&cwgY(GMNRr`VlX m,-{d*w7JNtH$Suq:g#~}3_M0Q=na%!0v|o4FQ.>SkIH*');
define('SECURE_AUTH_KEY',  '[^mgI%8lrCk6z% VI7@ab`6t-.=QEQ)-P _R|ybg>HW,Ht9o8@Wy|RY^k[I{&( S');
define('LOGGED_IN_KEY',    'OW~&w*+NqVkyY^q|z`kf-GfQ=O!(I nCEKnw*yF:nPQ?r?H]%da[Lj?fivN}z^gS');
define('NONCE_KEY',        'nqPzt vO;uJvEw:j1iXa{M|qN@5pkY;Ml_0|[j%y!H&P*R/L(*926vPGi+,VUyVd');
define('AUTH_SALT',        '1sk!^j@e5/I{WwMh*X^jbD+Qv[CS;w-iZi{4M0a^rV@f-HXm~uQsfdZ!B@b<U<#6');
define('SECURE_AUTH_SALT', '!`.X-Y.|9Y -F-Z|@y:iTAFX&u;.3^!]Ovfl,sg#-egQs.Fa#Kl3(LaRzwf=fOdj');
define('LOGGED_IN_SALT',   '> %4K]VJ.ALnts|~TD1gGQG^8t|&4PO-J+Ta]x!x(YS9#H~-{/TH0#sC8dp|F{u[');
define('NONCE_SALT',       '/60/e1,mBK,4`CG?=3xz`MeEO!].$m4B&gx;7+EPPyJ[mT;`6}(Sue4W:HS/`ckG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ecommerce_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** TWEAK TO UPGRADE PLUGINS AND CORE ON LAMP */
# define('FS_METHOD', 'direct');
